using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AJump
{
    public abstract void PerformJump(Rigidbody rb, float force);
    public abstract void PerformRotation(Rigidbody rb, float force);
}

public class Jump : AJump
{
    public override void PerformJump(Rigidbody rb, float force)
    {
        rb.AddForce(Vector3.up * force, ForceMode.Impulse);
    }

    public override void PerformRotation(Rigidbody rb, float force)
    {
        //
    }
}

public class JumpDecorator : AJump
{
    private AJump _ajump;

    public JumpDecorator(AJump j)
    {
        _ajump = j;
    }

    public override void PerformJump(Rigidbody rb, float force)
    {
        if (_ajump != null)
            this._ajump.PerformJump(rb, force);
    }

    public override void PerformRotation(Rigidbody rb, float force)
    {
        //
    }
}

public class RotationInJump : JumpDecorator
{
    public RotationInJump(AJump j) : base(j) { }

    public override void PerformJump(Rigidbody rb, float force)
    {
        base.PerformJump(rb, force);
    }

    public override void PerformRotation(Rigidbody rb, float force)
    {
        base.PerformRotation(rb, force);       
            rb.transform.eulerAngles += new Vector3(0, force, 0);
    }
}

public class FlipInJump : JumpDecorator
{
    public FlipInJump(AJump j) : base(j) { }

    public override void PerformJump(Rigidbody rb, float force)
    {
        base.PerformJump(rb, force);        
    }

    public override void PerformRotation(Rigidbody rb, float force)
    {
        base.PerformRotation(rb, force);
            rb.transform.eulerAngles += new Vector3(0, 0, force);
    }
}
