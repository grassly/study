using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    private Rigidbody rb;
    private bool isJumping;
    [SerializeField] private float rotationForce;
    private float _angle = 180;
    private float angle;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        isJumping = false;
        angle = _angle;
    }

    public void DoJump(float force)
    {
        AJump dojump = new Jump();
        dojump = new FlipInJump(dojump);
        dojump.PerformJump(rb, force);
        isJumping = true;
    }

    private void Update()
    {
        if (isJumping)
        {
            AJump dojump = new Jump();
            dojump = new FlipInJump(dojump);

            if (angle > 0)
            {
                dojump.PerformRotation(rb, rotationForce);
                angle -= rotationForce;
            }
            else
            {
                isJumping = false;
                angle = _angle;
            }
        }
    }

    public void OnEnable()
    {
        CapsuleBehaviour.OnSpaceInput += DoJump;
    }

    public void OnDisable()
    {
        CapsuleBehaviour.OnSpaceInput -= DoJump;
    }
}
