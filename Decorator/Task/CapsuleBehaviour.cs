using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleBehaviour : MonoBehaviour
{
    public static System.Action<float> OnSpaceInput;
    [SerializeField] private float jumpForce;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSpaceInput?.Invoke(jumpForce);
        }
    }
}
