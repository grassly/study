using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : MonoBehaviour
{
    private Rigidbody rb;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void DoJump(float force)
    {
        AJump dojump = new Jump();
        dojump.PerformJump(rb, force);
    }

    public void OnEnable()
    {
        CapsuleBehaviour.OnSpaceInput += DoJump;
    }

    public void OnDisable()
    {
        CapsuleBehaviour.OnSpaceInput -= DoJump;

    }
}
