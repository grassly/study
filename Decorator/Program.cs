using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ADebugable
{
    public abstract void Show(string s);
}

public class CustomDebuger : ADebugable
{
    public override void Show(string s)
    {
        Debug.Log(s);
    }
}

public class CustomDebugerDecorator : ADebugable
{
    private ADebugable _iDebugable;

    public CustomDebugerDecorator(ADebugable d)
    {
        _iDebugable = d;
    }

    /*public void SetDecorator(ADebugable d)
    {
        this._iDebugable = d;
    }*/

    public override void Show(string s)
    {
        if (this._iDebugable != null)
            this._iDebugable.Show(s);
    }
}

public class DecoratorA : CustomDebugerDecorator
{
    public DecoratorA(ADebugable d) : base(d) {  }

    public override void Show(string s)
    {
        base.Show(s);
        Debug.Log(s.Length);
    }
}

public class DecoratorB : CustomDebugerDecorator
{
    public DecoratorB(ADebugable d) : base(d) { }

    public override void Show(string s)
    {
        base.Show(s);
        System.IO.File.WriteAllText(@"C:\Users\Student\Documents\DecoratorString.txt", s);
    }
}

public class Program : MonoBehaviour
{
    private void Awake()
    {
        ADebugable text1 = new CustomDebuger();
        text1 = new DecoratorA(text1);
        text1.Show("Decorator A works!");

        ADebugable text2 = new CustomDebuger();
        text2 = new DecoratorB(text2);
        text2.Show("Decorator B works!");
    }
}