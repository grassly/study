using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetragon
{
    [SerializeField] private float oneSide;
    [SerializeField] private float anotherSide;
    [SerializeField] private float angleBetweenEm;

    public Tetragon(float a, float b, float angle) 
    {
        oneSide = a;
        anotherSide = b;
        angleBetweenEm = angle;
    }

    public float CountPerimeter(float a, float b)
    {
        return (a + b) * 2;
    }

    public virtual float CountSquare(float d1, float d2, float angle)
    {
        return 0;
        //return 0.5f * d1 * d2 * Mathf.Sin(angle);
    }
}

public class ConvexTetragon : Tetragon
{
    public ConvexTetragon(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float d1, float d2, float angle)
    {
        //return base.CountSquare(d1, d2, angle); 
        return 0.5f * d1 * d2 * Mathf.Sin(angle);
    }
}

public class Parallelogram : Tetragon
{
    public Parallelogram(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float a, float b, float angle)
    {
        return a * b * Mathf.Sin(angle);
    }
}

public class Rhombus : Parallelogram
{
    public Rhombus(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float d1, float d2, float angle)
    {
        //return base.CountSquare(a, b, angle);
        return d1 * d2 * 0.5f;
    }
}

public class Rectangle : Parallelogram
{
    public Rectangle(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float a, float b, float angle)
    {
        return a * b;
    }
}

public class Quad : Rectangle
{
    public Quad(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float a, float b, float angle)
    {
        return base.CountSquare(a, b, angle);
    }
}
