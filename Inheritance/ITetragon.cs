using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITetragon 
{
    float a { get; set; }
    float b { get; set; }
    float angle { get; set; }

    float CountPerimeter(float a, float b);
    float CountSquare(float d1, float d2, float angle);
}

public class ConvexITetragon : ITetragon
{
    public float a { get; set; }
    public float b { get; set; }
    public float angle { get; set; }

    public ConvexITetragon(float d1, float d2, float angleD)
    {
        a = d1;
        b = d2;
        angle = angleD;
    }

    public float CountPerimeter(float a, float b)
    {
        return (a + b) * 2;
    }

    public virtual float CountSquare(float d1, float d2, float angle)
    {
        return 0;
    }
}

public class ParallelogramITetragon : ConvexITetragon
{
    public ParallelogramITetragon(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float d1, float d2, float angle)
    {
        return base.CountSquare(d1, d2, angle);
    }
}

public class RhombusITetgagon : ConvexITetragon
{
    public RhombusITetgagon(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float d1, float d2, float angle)
    {
        return base.CountSquare(d1, d2, 90);
    }
}

public class RectangleITetragon : ConvexITetragon
{
    public RectangleITetragon(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float a, float b, float angle)
    {
        return a * b;
    }
}

public class QuadITetragon : ConvexITetragon
{
    public QuadITetragon(float a, float b, float angle) : base(a, b, angle) { }

    public override float CountSquare(float a, float b, float angle)
    {
        return base.CountSquare(a, b, angle);
    }
}
