using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Some Card", fileName ="SomeCardAsset")]
public class CardAsset : ScriptableObject
{
    public string Name; //{ set { Name = name; } }
    public Color Color;
    //public Sprite Image;
    public Texture2D texture;
    public string Description;

    public void OnValidate()
    {
        Name = name;//?
    }

}
