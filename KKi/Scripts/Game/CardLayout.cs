using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardLayout : MonoBehaviour
{
    //[RequireComponent(RectTransform)]
    public int LayoutId;
    public Vector2 Offset;
    private CardGame cardGame;
    [SerializeField] private bool FaceUp;
    private RectTransform rectTransform;
    public float Scale;

    private List<CardView> cardsOnDeck = new List<CardView>();

    private int currSiblingId;

    void Start()
    {
        currSiblingId = 0;
        cardGame = FindObjectOfType<CardGame>();
        rectTransform = transform.GetComponent<RectTransform>();
        //Debug.Log(rectTransform.rect.width);
    }

    void Update()
    {
        //Debug.Log(cardsOnDeck.Count);

        foreach (CardView keyValue in cardGame.GetCardsInLayout(LayoutId))
        {
            cardsOnDeck = cardGame.GetCardsInLayout(LayoutId);
            cardGame.RecalculateLayout(LayoutId); //������� ����������� ����� ����� layout���
            //cardGame.RecalculateLayout(LayoutId);
            //keyValue.Value.RotateCard(FaceUp);
            //Offset = new Vector3(Mathf.Round(rectTransform.rect.width / (cardGame.GetCardsInLayout(LayoutId).Count * Scale)), 0, 0);

            

            var cardViewRect = keyValue.transform.GetComponent<RectTransform>();
            keyValue.transform.SetParent(transform);

            var multiplier = -cardsOnDeck.Count / 2; //�� ����� ������� �� �����
            if (cardsOnDeck.Count % 2 != 0)
            {
                if (multiplier == 0)
                    multiplier = 1;
            }

            cardViewRect.localPosition = new Vector3(multiplier * Offset.x, 0, 0);
            multiplier++;

            
            cardViewRect.SetSiblingIndex(transform.childCount);
            
            //currSiblingId++;
        }

        //Rotate(FaceUp); //������� ���������� ����� �� CardView
    }
}
