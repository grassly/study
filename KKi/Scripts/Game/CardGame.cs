using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGame : Singleton<CardGame>
{
    public List<CardAsset> allCardAssets = new List<CardAsset>();
    public Dictionary<CardInstance, CardView> cardsInstanceView = new Dictionary<CardInstance, CardView>();

    [SerializeField] private GameObject cardPrefab;

    [SerializeField] private LayoutGroup layout;
    [SerializeField] private int deckLayoutId;
    [SerializeField] private int startLayoutId;

    [SerializeField] private int HandCapacity;
    [SerializeField] private int AmountOfPlayers;

    private int currPlayer;
    [SerializeField] private int playerDeckLayoutId;
    [SerializeField] private int playerHandLayoutId;
    [SerializeField] private int playerDropLayoutId;

    [SerializeField] private int enemyDeckLayoutId;
    [SerializeField] private int enemyHandLayoutId;
    [SerializeField] private int enemyDropLayoutId;


    private void Awake()
    {
        //var arr = Resources.FindObjectsOfTypeAll<CardAsset>();
        //Debug.Log(arr.Length);

        //foreach (CardAsset card in arr)
            //allCardAssets.Add(card);

        StartGame();
    }

    public void StartGame()
    {
        foreach (CardAsset card in allCardAssets)
        {
            //Debug.Log(card.Name);
            CreateCardInstance(card, playerDeckLayoutId);
            CreateCardInstance(card, enemyDeckLayoutId);
        }

        ShuffleLayout(playerDeckLayoutId);
        ShuffleLayout(enemyDeckLayoutId);

        currPlayer = 0;

        StartTurn();
    }

    public void CreateCardInstance(CardAsset card, int layoutId)
    {
        //���������� cardInstance
        //instance.Init(card);//?
        var newCardInstance = new CardInstance(card);
        CreateCardView(newCardInstance);
        newCardInstance.MoveToLayout(layoutId);
        //instance.MoveToLayout(layout);

        //return newCardInstance;
    }

    public void CreateCardView(CardInstance newCardInstance)
    {
        //�������� ������� � ������������� ������� ����� cardInstance
        GameObject cardSample = Instantiate(cardPrefab);
        CardView newcardView = cardSample.GetComponent<CardView>();
        newcardView.Init(newCardInstance);
        cardsInstanceView.Add(newCardInstance, newcardView);


        #region meh
        /*var currCardText = currPrefab.GetComponentsInChildren<Text>();
        currCardText[0].text = newCardInstance.CardAsset.Name;
        currCardText[1].text = newCardInstance.CardAsset.Description;

        var currCardImage = currPrefab.GetComponentInChildren<Image>();
        currCardImage.sprite = newCardInstance.CardAsset.Image;

        var currPrefabColor = currPrefab.GetComponent<Image>();
        currPrefabColor.color = newCardInstance.CardAsset.Color;*/
        #endregion
    }


    public List<CardView> GetCardsInLayout(int LayoutId)
    {
        List<CardView> listOfCardsInLayout = new List<CardView>();
        foreach (CardView keyValue in cardsInstanceView.Values)
        {
            if (keyValue.CardInstance.LayoutId == LayoutId)
            {
                listOfCardsInLayout.Add(keyValue);
            }
        }
        return listOfCardsInLayout;
    }

    public void RecalculateLayout(int layoutId)
    {
        List<CardView> listOfCardsInLayout = GetCardsInLayout(layoutId);
        for (int i = 0; i < listOfCardsInLayout.Count; i++)
        {
            listOfCardsInLayout[i].CardInstance.CardPosition = i + 1;
            //Debug.Log(i + 1);
            //Debug.Log(GetCardsInLayout(layoutId).Count);
        }
    }

    public void StartTurn()
    {
        for (int i = 0; i < AmountOfPlayers; i++)
        {
            for (int j = 0; j < HandCapacity; j++)
            {
                List<CardView> listOfCardsInLayout = GetCardsInLayout(deckLayoutId);
                listOfCardsInLayout[0].CardInstance.MoveToLayout(i);
            }
        }
    }

    public void ShuffleLayout(int layoutId)
    {
        List<CardView> listOfCardsInLayout = GetCardsInLayout(layoutId);
        for (int i = 0; i < listOfCardsInLayout.Count * 2; i++)
            listOfCardsInLayout[i].CardInstance.CardPosition = Random.Range(0, listOfCardsInLayout.Count);

        RecalculateLayout(layoutId);
    }

    public void EndTurn()
    {
        List<CardView> CardsInLayout = GetCardsInLayout(0);
        for (int i = 0; i < CardsInLayout.Count; i++)
        {
            if (currPlayer == 0)
                CardsInLayout[i].CardInstance.MoveToLayout(playerDeckLayoutId);
            else
                CardsInLayout[i].CardInstance.MoveToLayout(enemyDeckLayoutId);

        }

        if (currPlayer == 0) currPlayer = 1;
        else currPlayer = 0;

        if (GetCardsInLayout(playerDeckLayoutId).Count <= 0)
            RefillDeck(playerDropLayoutId, playerDeckLayoutId);
        else if (GetCardsInLayout(enemyDeckLayoutId).Count <= 0)
            RefillDeck(enemyDropLayoutId, enemyDeckLayoutId);

            StartTurn();
    }

    public void RefillDeck(int previousLayoutId, int nextLayoutId)
    {
        List<CardView> cardsInLayout = GetCardsInLayout(previousLayoutId);
        for (int i = 0; i < cardsInLayout.Count; i++)
            cardsInLayout[i].CardInstance.MoveToLayout(nextLayoutId);
        ShuffleLayout(nextLayoutId);

    }
}
