using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;


public class CardView : MonoBehaviour, IPointerClickHandler
{
    public CardInstance CardInstance;
    private Animator cardAnim;
    [SerializeField] private Text cardName;
    [SerializeField] private Image cardColor;
    [SerializeField] private Image cardImage;
    [SerializeField] private int pos;

    [SerializeField] private int CenterLayoutId;
    [SerializeField] private int HandLayoutId;

    public void Init(CardInstance someCardInstance)
    {
        CardInstance = someCardInstance;
        cardName.text = CardInstance.CardAsset.Name;
        cardColor.color = CardInstance.CardAsset.Color;
        cardImage.sprite = Sprite.Create(CardInstance.CardAsset.texture,
            new Rect(0.0f, 0.0f, CardInstance.CardAsset.texture.width,
            CardInstance.CardAsset.texture.height),
            new Vector2(0.5f, 0.5f), 100.0f);
    }

    public void Awake()
    {
        cardAnim = transform.GetComponent<Animator>();
    }

    public void RotateCard(bool up)
    {
        if (up)
            cardAnim.SetFloat("Speed", 1);        
        else
            cardAnim.SetFloat("Speed", -1);

       cardAnim.Play("RotateCard");
    }

    private void Update()
    {
        pos = transform.GetSiblingIndex();
        //CardInstance.LayoutId = id;
       
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        
        //����������� ����� ����� layouts
        if (CardInstance.LayoutId == HandLayoutId)
        {
            PlayCard(CenterLayoutId);
        }
        else if (CardInstance.LayoutId == CenterLayoutId)
        {
            PlayCard(HandLayoutId);
        }

        
    }

    public void PlayCard(int layoutId)
    {
        CardInstance.LayoutId = layoutId;
        //CardInstance.CardPosition = 0;
        //transform.SetSiblingIndex(0);
        //if (FindObjectOfType<CardLayout>().LayoutId == layoutId)

        //transform.SetSiblingIndex();
    }
}
