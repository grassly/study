using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCP_Test : MonoBehaviour
{
    public string message;

    public TCP_Client client;
    public TCP_Server server;

    void Start()
    {
        client.StartClient();
        server.StartServer();

        //create socket
        //socket.Listen(int a);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            client.SendTcpMessage(message);
            server.SendTcpMessage(message);
        }
    }
}
