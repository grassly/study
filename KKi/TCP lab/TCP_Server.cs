using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class TCP_Server : MonoBehaviour
{
    private int Port = 8889;

    private TcpClient connectedClient;
    private Thread tcpListenerThread;
    private TcpListener tcpListener;

    public Action<string> onMessageRecieved = (v) => { };

    public void StartServer()
    {
        //this.Port = Port;

        tcpListenerThread = new Thread(new ThreadStart(ListenForIncomingRequests));
        tcpListenerThread.IsBackground = true;
        tcpListenerThread.Start();
    }

    private void ListenForIncomingRequests()
    {
        try
        {
            tcpListener = new TcpListener(IPAddress.Any, Port);
            tcpListener.Start();

            byte[] bytes = new byte[1024];

            while (true)
            {
                using (connectedClient = tcpListener.AcceptTcpClient())
                {
                    using (NetworkStream stream = connectedClient.GetStream())
                    {
                        int length;

                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            string clientMessage = Encoding.ASCII.GetString(incommingData);
                            foreach (string cmd in clientMessage.Split('/'))
                            {
                                if (cmd != string.Empty)
                                {
                                    Debug.Log("Listened message: " + cmd);
                                    MainThreadDispatcher.Instance.InvokeInMainThread(() => onMessageRecieved(cmd));
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (SocketException ex)
        {
            Debug.Log("SocketException: " + ex.ToString());
        }
    }

    public void SendTcpMessage(string msg)
    {
        SendTcpMessage(msg, new List<string>());
    }

    public void SendTcpMessage(string msg, List<string> parameters)
    {
        if (connectedClient == null)
        {
            return;
        }

        try
        {
            NetworkStream stream = connectedClient.GetStream();

            if (stream.CanWrite)
            {
                string serverMessage = msg + ",";

                foreach (string f in parameters)
                {
                    serverMessage += f + ",";
                }

                serverMessage += "/";
                byte[] serverMessageBytesArray = Encoding.ASCII.GetBytes(serverMessage);
                Debug.Log("server" + serverMessageBytesArray);
                stream.Write(serverMessageBytesArray, 0, serverMessageBytesArray.Length);

            }
        }
        catch (SocketException ex)
        {
            Debug.Log("SocketException: " + ex.ToString());
        }
    }

    private void OnDestroy()
    {
        tcpListenerThread.Abort();
        tcpListenerThread = null;
        connectedClient?.Close();
        connectedClient = null;
        tcpListener.Stop();
        tcpListener = null;
    }
}
