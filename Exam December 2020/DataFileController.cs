﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataFileController : MonoBehaviour
{
    [SerializeField]private GameData scriptableObject;
    private string fullPath;
    private string fileName = "Data.txt";
    private string pathToFile;
    private char dots = ':';
    public float savedVolume;
    public int savedDifficulty;

    void Update()
    {
        Save();
        Load();
    }

    public void Save()
    {
        pathToFile = Application.persistentDataPath;
        fullPath = Path.Combine(pathToFile, fileName);
        File.WriteAllText(fullPath, scriptableObject.Volume.ToString() + dots + scriptableObject.Difficulty.ToString());
    }

    public void Load()
    {
        string data = File.ReadAllText(fullPath);
        string[] dataInfo;
        dataInfo = data.Split(dots);
        savedVolume = float.Parse(dataInfo[0]);
        savedDifficulty = int.Parse(dataInfo[1]);
    }
}
