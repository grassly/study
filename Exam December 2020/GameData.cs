﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="GameData", menuName = "Settings")]
public class GameData : ScriptableObject
{
    [SerializeField] private float _volume;
    [SerializeField] private int _difficulty;

    public float Volume => _volume;
    public int Difficulty => _difficulty;
}
