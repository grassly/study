﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataGetter : MonoBehaviour
{
    public DataFileController dfc;
    [SerializeField] private AudioSource audioSource;

    void Update()
    {
        Debug.Log(dfc.savedDifficulty);

        SetVolume();
    }

    private void SetVolume()
    {
        audioSource.volume = dfc.savedVolume;
    }
}
