using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class TCP_Client : MonoBehaviour
{
    [SerializeField] private int Port = 8888;
    [SerializeField] private string IP = "192.168.1.66";

    public TcpClient newClient;
    private Thread clientReceiveThread;

    public Action<string> onMessageRecieved = (v) => { };

    public void StartClient()
    {
        //someClient = new TcpClient(IP, Port);
        IPAddress ipAdd = IPAddress.Parse(IP);
        IPEndPoint endpoint = new IPEndPoint(ipAdd, Port);
        newClient = new TcpClient(endpoint);

        clientReceiveThread = new Thread(new ThreadStart(ListenForData));
        clientReceiveThread.IsBackground = true;
        clientReceiveThread.Start();

        ConnectClient(ipAdd);
    }

    public void ConnectClient(IPAddress ipAdd)
    {
        try
        {
            newClient.Connect(ipAdd, Port);
        }
        catch (SocketException ex)
        {
            Debug.Log("Connection exception: " + ex.ToString());
        }
    }

    private void ListenForData()
    {
        try
        {
            byte[] bytes = new byte[1024];

            while(true)
            {
                /*try
                {
                    NetworkStream streamm = newClient.GetStream();
                }
                catch (InvalidOperationException ex)
                {
                    Debug.Log(ex);
                }*/

                NetworkStream stream = newClient.GetStream();
                int length;

                while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                {
                    //int byteRead = stream.Read(bytes, 0, newClient.ReceiveBufferSize);
                    var returnData = new byte[length];
                    Array.Copy(bytes, 0, returnData, 0, newClient.ReceiveBufferSize);
                    string incomingData = Encoding.ASCII.GetString(returnData);

                    foreach (string cmd in incomingData.Split('/'))
                    {
                        if (cmd != string.Empty)
                            Debug.Log(cmd); //=> onMessageRecieved(cmd));
                    }
                }           
            }

        }
        catch (SocketException ex)
        {
            Debug.Log("Socket exception: " + ex);
        }
    }

    public void SendTcpMessage(string clientMessage)
    {
        SendTcpMessage(clientMessage, new List<string>());
    }

    private void SendTcpMessage(string clientMessage, List<String> parameters)
    {
        if (newClient == null)
            return;
        try
        {
            string message = clientMessage + ",";
            foreach (string f in parameters)
            {
                message += f + ",";
            }
            message += "/";

            NetworkStream stream = newClient.GetStream();
            if (stream.CanWrite)
            {
                byte[] clientMessageBytesArray = Encoding.ASCII.GetBytes(message);
                Debug.Log("client" + clientMessageBytesArray);
                stream.Write(clientMessageBytesArray, 0, clientMessageBytesArray.Length);
            }
        }
        catch (SocketException ex)
        {
            Debug.Log("Socket exception: " + ex);
        }
    }

    private void OnDestroy()
    {
        clientReceiveThread.Abort();
        clientReceiveThread = null;
        newClient?.Close();
        newClient = null;
    }

}
