using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TCP_Test : MonoBehaviour
{
    [SerializeField] private string message;

    TCP_Client client;
    TCP_Server server;
    public GameObject serverGO;

    void Start()
    {
        client = GetComponent<TCP_Client>();
        client.StartClient();

        server = serverGO.GetComponent<TCP_Server>();
        server.StartServer();

        //create socket
        //socket.Listen(int a);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            //client.SendMessage(message);
            //server.SendMessage(message);
        }
    }
}
