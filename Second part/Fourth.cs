﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Fourth : MonoBehaviour
{
    public Text texttt;
    private string mark;
    private int score;
    private int scor;


    void Start()
    {
        score = UnityEngine.Random.Range(60, 101);
        if (score < 70) 
            scor = 0;
        if ((score >= 70) && (score < 80)) 
            scor = 1;
        if (score >= 80)
            scor = 2;

        switch (scor)
        {
            case 0:
                mark = "удовлетворительно";
                break;
            case 1:
                mark = "хорошо";
                break;
            case 2:
                mark = "отлично";
                break;   
        }

        texttt.text = $"За {score} баллов вы получили {mark}"; 
        
    }

    void Update()
    {
        
    }
}
