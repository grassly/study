﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Six : MonoBehaviour
{
    public int a;
    public int b;
    private char[] arrA;
    private char[] arrB;
    private List<char> nums = new List<char>();
    private int count;

    void Start()
    {
        string aa = a.ToString();
        string bb = b.ToString();

        char[] arrA = new char[aa.Length];
        char[] arrB = new char[bb.Length];

        for (int i = 0; i < aa.Length; i++)
        {
            arrA[i] = aa[i];
        }

        for (int i = 0; i < bb.Length; i++)
        {
            arrB[i] = bb[i];
        }

        for (int i = 0; i < aa.Length; i++)
        {
            for (int j = 0; j < bb.Length; j++)
            {
                if (arrA[i] == arrB[j])
                {
                    nums.Add(arrA[i]);
                    count++;
                    //Debug.Log(arrA[i]);
                }
            }
        }

        for (int i = 0; i < count - 1; i++)
        {
            if (nums[i] == nums[i + 1])
            {
                nums.Remove(nums[i]);
                count--;
            }

        }

        for (int i = 0; i < count; i++)
        {
            Debug.Log(nums[i]);
        }
    }

    void Update()
    {

    }
}

