﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class First : MonoBehaviour
{
    private int sum;
    private int i;

    void Start()
    {
        i = 0;
        sum = 0;
        while (i < 13)
        {
            if ((i % 2 > 0 || i / 2 == 1) && (i % 3 > 0 || i / 3 == 1))
            {
                Debug.Log(i);
                sum += i;
            }
            i++;
        }
        Debug.Log(sum);
    }

    void Update()
    {
        
    }
}
