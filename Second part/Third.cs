﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Third : MonoBehaviour
{
    public Text text;
    //private int m;

    void Start()
    {
        for (int i = 0; i < 5; i++)
        {
            if (i % 2 == 0)
            {               
                text.text += "1";
                for (int n = 0; n < i; n++)
                {
                    if (n % 2 == 0)
                        text.text += "0";
                    else
                        text.text += "1";
                }
                
            }
            else
            {
                text.text += "0";
                for (int n = 0; n < i; n++)
               {            
                    if (n % 2 == 0)
                        text.text += "1";
                    else
                        text.text += "0";}
            }
            text.text += "\r\n";
        }
    }

    void Update()
    {
        
    }
}
