﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SixthC : MonoBehaviour
{
    private int n;
    private int sum;
    private bool isSimple;

    private List<int> simple = new List<int> ();
    
    void Start()
    {
        n = 1700;
        sum = 1 + 2 + 3;

        for (int i = 4; i < n; i++) //перебираем числа
        {
            CountSimple(i);
            if (isSimple == false)
                sum += i;
        }

        Debug.Log(sum);
    }

    void Update()
    {
        
    }

    public bool CountSimple(int x) //получаем число и перебираем все делители от 2 до n / 2
    { 
        for (int i = 2; i < x - 1 ; i++)
        {
            if (x % i == 0)
            {
                isSimple = true;
                break;
            }
            else
                isSimple = false;
        }
        return isSimple;
    }
}
