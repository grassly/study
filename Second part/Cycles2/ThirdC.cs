﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdC : MonoBehaviour
{
    private int maxGuest = 5;
    private int minGuest = 17;
    private int guests;
    private int days = 88;
    private int currDay;
    private int unlucky = 173;
    private int aLot = 200;
    private int extraGuest = 100;

    private string marriage;

    void Start()
    {
        guests = 0;
        marriage = "Свадьба состоялась!";

        while (currDay < days)
        {
            guests += Random.Range(minGuest, maxGuest + 1);
            currDay++;

            if (guests > aLot)
                guests -= extraGuest;

            if (guests == unlucky)
            {
                marriage = "Свадьба не состоялась :(";
                break;
            }
        }

        Debug.Log(marriage);
    }

    void Update()
    {
        
    }
}
