﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourthC : MonoBehaviour
{
    private int levels = 33;
    private int soldiers;
    private int dead = 66;
    private int lastLevel;

    void Start()
    {
        soldiers = 1000;

        for (int i = 0; i < levels; i++)
        {
            if (i % 2 == 1) //может показаться, что должно быть наоборот, здесь без остатка, но когда этаж = 1, i = 0: этаж нечетный - номер четный
            {
                soldiers -= dead;
            }
            else if (i % 2 == 0)
            {
                soldiers -= soldiers / 10;
            }

            //Debug.Log(soldiers + " " + i);
                
            if (soldiers < 0 || soldiers == 0)
            {
                lastLevel = i + 1; //ибо первый этаж имеет в цикле №1
                break;
            }
        }

        Debug.Log(lastLevel);
    }

    void Update()
    {
        
    }
}
