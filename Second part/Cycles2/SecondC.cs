﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondC : MonoBehaviour
{
    private int days = 100;
    private int sum;
    private int cars;
    private bool directorComes;

    void Start()
    {
        sum = 0;
        cars = 0;
        //directorComes = true;

        for (int i = 0; i < days; i++)
        {
            if (cars <= 20)
            {
                directorComes = true;
            } else
                directorComes = false;

            switch (directorComes)
            {
                case true:
                    cars += 7;
                    sum += 7;
                    break;
                case false:
                    cars = 0;
                    sum++;
                    break;
            }       
        }
        Debug.Log(sum);
    }

    void Update()
    {
        
    }
}
