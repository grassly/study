﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstC : MonoBehaviour
{
    private int maxPeopleOnFloor = 3;
    private int minPeopleOnFloor = 9;
    private int people;
    private int floor;
    private int floors = 45;


    void Start()
    {
        floor = 0;
        while (floor < floors)
        {
            people += Random.Range(minPeopleOnFloor, maxPeopleOnFloor + 1);
            floor++;
        }
        Debug.Log(people);
    }

    void Update()
    {
        
    }
}
