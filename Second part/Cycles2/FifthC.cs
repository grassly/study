﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FifthC : MonoBehaviour
{
    private int boryaHealth;
    private int enemyHealth;

    private int shootsToBorya = 91;
    private int shootsToEnemy = 3;

    private int rounds;
    private string boryaStatus;

    void Start()
    {
        boryaHealth = 800;
        enemyHealth = 1200;
        rounds = 0;

        while (shootsToBorya > 0)
        {       
                shootsToBorya--;
                boryaHealth -= Random.Range(3, 18);
                for (int i = 0; i < shootsToEnemy; i++)
                    enemyHealth -= Random.Range(4, 8);
                rounds++;
        }

        if (boryaHealth >= 0)
            boryaStatus = "win";
        else
            boryaStatus = "lose";

        Debug.Log($"Borya {boryaStatus} after {rounds}th round!");
    }

    void Update()
    {
        
    }
}
