using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceBank : MonoBehaviour
{
    public static ResourceBank resourceBankInstance
    {
        get
        {
            if (resourceBankInstance == null)
            {
                resourceBankInstance = FindObjectOfType<ResourceBank>();

                if (resourceBankInstance == null)
                {
                    var go = new GameObject("ResourceBank");
                    go.AddComponent<ResourceBank>();
                    resourceBankInstance = go.GetComponent<ResourceBank>();
                }
            }
            return resourceBankInstance;
        }
        set
        { }
    }

    public enum ResType
    {
        WOOD,
        WORKER,
        HOUSE,
        MONEY
    }

    private Dictionary<ResType, int> resources = new Dictionary<ResType, int>();

    public static System.Action<ResType, int> OnResChanged;

    [SerializeField] private float coroutinePeriod = 0.5f;

    void Awake()
    {

        FillDictionary();

        StartCoroutine(GetWood());
    }

    public void FillDictionary()
    {
        foreach (ResType item in (ResType[])System.Enum.GetValues(typeof(ResType)))
        {
            if (item.ToString() != "WORKER")
                resources.Add(item, 0);
            else
                resources.Add(item, 2);

            //Debug.Log(suit);
        }

        /*foreach (KeyValuePair<ResType, int> item in resources)
        {
            Debug.Log($"Key: {item.Key}; Value: {item.Value}");
        }*/
    }

    public void ChangeResource(ResType res, int val)
    {
        resources[res] = val;
        OnResChanged?.Invoke(res, val);
    }

    public int GetResource(ResType res)
    {
        return resources[res];
    }

    public IEnumerator GetWood()
    {
        yield return new WaitForSeconds(coroutinePeriod);
        ChangeResource(ResType.WOOD, resources[ResType.WOOD] + resources [ResType.WORKER]);
    }
}
