using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CommandInvoker
{
    private Queue<ICommand> commands;
    private Queue<ICommand> completedCommands;

    public CommandInvoker()
    {
        commands = new Queue<ICommand>();
    }

    public void AddCommand(ICommand command, Transform t, float f)
    {
        if (command != null)
        {
            commands.Enqueue(command);
            command.Description.Add(t, f);
        }
    }

    public void RemoveCommand(ICommand command)
    {
        if (command != null)
        {
            ICommand[] toRemove = new ICommand[] { command };
            commands = new Queue<ICommand>(commands.Where(x => toRemove.Contains(x)));
        }
    }

    public void ProcessAll()
    {
        foreach (ICommand command in commands)
        {
            Process();
        }
    }

    public void Process()
    {
        if (commands != null)
        {
            var command = commands.Dequeue();
            command.Invoke(command.Description.ElementAt(0).Key, command.Description.ElementAt(0).Value);
            completedCommands.Enqueue(command);
        }
    }

    public void Undo()
    {
        var command = completedCommands.Dequeue();
        command.Undo(command.Description.ElementAt(0).Key, command.Description.ElementAt(0).Value);
    }

    public void ClearQueue(Queue<ICommand> commands)
    {
        foreach (ICommand command in commands)
        {
            RemoveCommand(command);
        }
    }

    public string DebugCheck()
    {
        return $"To do: {commands.Count()}; completed: {completedCommands.Count()}";
    }

}
