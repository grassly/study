using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICommand
{
    Dictionary<Transform, float> Description { get; set; }

    public void Invoke(Transform t, float dist);
    public void Undo(Transform t, float angle);
}

public class MoveForward : ICommand
{
    public Dictionary<Transform, float> Description { get; set; }

    Transform t;
    float dist;

    public MoveForward(Transform t, float dist)
    {
        this.t = t;
        this.dist = dist;
    }

    public void Invoke(Transform t, float dist)
    {
        t.position = new Vector3(0, t.position.y + dist, 0);
    }

    public void Undo(Transform t, float dist)
    {
        t.position = new Vector3(0, t.position.y - dist, 0);
    }
}

public class Rotate : ICommand
{
    public Dictionary<Transform, float> Description { get; set; }

    Transform t;
    float angle;

    public Rotate(Transform t, float angle)
    {
        this.t = t;
        this.angle = angle;
    }

    public void Invoke(Transform t, float angle)
    {
        t.transform.eulerAngles += new Vector3(0, angle, 0);
    }

    public void Undo(Transform t, float angle)
    {
        t.transform.eulerAngles -= new Vector3(0, angle, 0);
    }
}
