using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandController : MonoBehaviour
{
    private CommandInvoker invoker;

    [SerializeField] private float distance = 1;
    [SerializeField] private float angle = 90;

    private void OnEnable()
    {
        invoker = new CommandInvoker();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            var command = new MoveForward(this.transform, distance);
            invoker.AddCommand(command, this.transform, distance);
            Debug.Log(invoker.DebugCheck());
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            var command = new Rotate(this.transform, angle);
            invoker.AddCommand(command, this.transform, angle);
            Debug.Log(invoker.DebugCheck());
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            var command = new Rotate(this.transform, -angle);
            invoker.AddCommand(command, this.transform, -angle);
            Debug.Log(invoker.DebugCheck());
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            invoker.ProcessAll();
            Debug.Log(invoker.DebugCheck());
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            invoker.Process();
            Debug.Log(invoker.DebugCheck());
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            invoker.Undo();
            Debug.Log(invoker.DebugCheck());
        }
    }
}
