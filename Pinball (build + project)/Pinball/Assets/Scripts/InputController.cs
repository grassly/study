﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputController : Singleton<InputController>
{
    [HideInInspector]public int shoots;
    public GameObject ballPrefab;
    public float force;

    void Start()
    {
        shoots = 0;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            if (shoots < 3)
                Shoot();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("pinball");
        }
    }

    public void Shoot()
    {
        var currBall = Instantiate(ballPrefab, transform.position, Quaternion.Euler(Vector3.zero));
        var rb = currBall.GetComponent<Rigidbody>();
        rb.AddForce(new Vector3(0, force, force), ForceMode.Impulse);
        shoots++;
    }

}
