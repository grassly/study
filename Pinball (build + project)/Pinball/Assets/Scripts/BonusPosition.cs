﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusPosition : MonoBehaviour
{
    [HideInInspector] public Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Plane"))
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
    }
}
