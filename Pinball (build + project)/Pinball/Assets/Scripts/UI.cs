﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : Singleton<UI>
{
    [HideInInspector] public int score;
    [HideInInspector] public Text scoreText;

    [HideInInspector] public Animator textAnimator;

    InputController ic;

    public GameObject[] balls;

    public GameObject reload;

    void Start()
    {
        scoreText = GetComponentInChildren<Text>();
        textAnimator = GetComponent<Animator>();
        ic = InputController.Instance;
        reload.SetActive(false);
    }

    void Update()
    {
        scoreText.text = $"Score: {score}";
        /*switch (ic.shoots)
        {
            case 0:
                balls[0].SetActive(false);
                return;
            case 1:
                balls[1].SetActive(false);
                return;
            case 2:
                balls[2].SetActive(false);
                return;
        }*/
        if (ic.shoots > 0)
            balls[ic.shoots - 1].SetActive(false);
        if (ic.shoots == 3)
            reload.SetActive(true);
    }
}
