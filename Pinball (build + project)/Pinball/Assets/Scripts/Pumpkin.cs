﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pumpkin : MonoBehaviour
{
    [HideInInspector] public Rigidbody rb;
    public float force;

    Ball ball;
    [HideInInspector] public Vector3 lVelocity;

    private int bonus;
    public GameObject bonusPrefab;

    public GameObject jack;
    [HideInInspector] public Animator jackAnim;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        ball = Ball.Instance;
        jackAnim = jack.GetComponent<Animator>();
    }

    public void Update()
    {
        lVelocity = ball.lastVelocity;
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            jackAnim.SetTrigger("Ouch!");
            var ball = other.gameObject.GetComponent<Rigidbody>();
            var speed = lVelocity.magnitude;
            var direction = Vector3.Reflect(lVelocity.normalized, other.contacts[0].normal);
            ball.velocity = direction * Mathf.Max(speed, force);
            bonus = Random.Range(3, 8);
            //Debug.Log(bonus);
            for (int i = 0; i < bonus; i++)
            {
                StartCoroutine(BonusSpawn());
            }
        }
    }

    public IEnumerator BonusSpawn()
    {
        yield return new WaitForSeconds(Random.Range(0.2f, 1));
        var bonusVector = Random.insideUnitSphere;
        var currBonus = Instantiate(bonusPrefab, transform.position, Quaternion.Euler(Vector3.zero));
        var currBonusRb = currBonus.GetComponent<Rigidbody>();
        var bonusImpulse = Random.Range(1, 3);
        currBonusRb.AddForce(bonusVector * bonusImpulse, ForceMode.Impulse);
        //Debug.Log(bonusVector + " " + bonusImpulse);
    }
}
