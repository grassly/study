﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderWalk : MonoBehaviour
{
    [SerializeField] private Transform[] routes;
    private int routeToGo;
    private float tParam;
    private Vector3 currPos;
    private float speedM;
    private bool allowC;

    private void Start()
    {
        routeToGo = 0;
        tParam = 0;
        speedM = 0.5f;
        allowC = true;
    }

    private void Update()
    {
        if (allowC)
            StartCoroutine(Go(routeToGo));

     }

    private IEnumerator Go(int routeNum)
    {
        if (gameObject.CompareTag("Spider"))
        {
            yield return new WaitForEndOfFrame();
        }
        allowC = false;
        Vector3 v0 = routes[routeNum].GetChild(0).position;
        Vector3 v1 = routes[routeNum].GetChild(1).position;
        Vector3 v2 = routes[routeNum].GetChild(2).position;
        Vector3 v3 = routes[routeNum].GetChild(3).position;

        while (tParam < 1)
        {
            tParam += Time.deltaTime * speedM;

            currPos = Mathf.Pow(1 - tParam, 3) * v0 +
                3 * Mathf.Pow(1 - tParam, 2) * tParam * v1 +
                3 * (1 - tParam) * Mathf.Pow(tParam, 2) * v2 +
                Mathf.Pow(tParam, 3) * v3;

            transform.position = currPos;
            yield return new WaitForEndOfFrame();
        }
        tParam = 0;
        routeToGo++;

        if (routeToGo > routes.Length - 1)
            routeToGo = 0;

        allowC = true;
    }

}
