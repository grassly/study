﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceObject : MonoBehaviour
{
    [HideInInspector] public Rigidbody rb;
    public float force;
    [HideInInspector] public Ball lVel;
    [HideInInspector] public Vector3 lVelocity;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        lVel = Ball.Instance;
    }

    public void Update()
    {
        lVelocity = lVel.lastVelocity; 
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var ball = other.gameObject.GetComponent<Rigidbody>();
            var speed = lVelocity.magnitude;
            var direction = Vector3.Reflect(lVelocity.normalized, other.contacts[0].normal);
            ball.velocity = direction * Mathf.Max(speed, force);
        }
    }
}
