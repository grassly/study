﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : Singleton<Ball>
{
    [HideInInspector]public Vector3 lastVelocity;
    [HideInInspector]public Rigidbody rb;

    UI ui;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        ui = UI.Instance;
    }

    void Update()
    {
        lastVelocity = rb.velocity;
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bonus"))
        {
            ui.score++;
            ui.textAnimator.SetTrigger("Bonus!");
            Destroy(other.gameObject);
        }
    }
}
