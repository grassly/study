﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipController : MonoBehaviour
{
    public HingeJoint hinge;

    void Start()
    {
        hinge = GetComponent<HingeJoint>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            hinge.useMotor = true;

        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            hinge.useMotor = false;
        }
    }
}
