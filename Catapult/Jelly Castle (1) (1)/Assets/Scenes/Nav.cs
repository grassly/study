﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Nav : MonoBehaviour
{


    public void Go()
    {
        SceneManager.LoadScene("Catapulta");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
