﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orange : MonoBehaviour
{
    public Rigidbody hand;
    public float breakJointTime;
    public GameObject Arm;

    void Start()
    {
        Arm = GameObject.Find("GoldCatapultArm");
        hand = Arm.GetComponent<Rigidbody>();
        this.GetComponent<FixedJoint>().connectedBody = hand;
        breakJointTime = 0.02f;
    }

    void Update()
    {
            if (Input.GetKeyDown(KeyCode.RightShift))
            {
                
                if (this.GetComponent<FixedJoint>() != null)
                    this.GetComponent<FixedJoint>().breakForce = 100000;
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Invoke("BreakeJoint", breakJointTime);

            }
        
    }

    public void BreakeJoint()
    {
        if (this.GetComponent<FixedJoint>() != null)
            this.GetComponent<FixedJoint>().breakForce = 0;
    }
}
