﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Reloader : MonoBehaviour
{
    public GameObject buttons;
    public GameObject button1;
    public GameObject button2;

    private int counter;

    void Start()
    {
        buttons.SetActive(false);
        button1.SetActive(false);
        button2.SetActive(false);
        counter = 0;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            counter++;
            if (counter % 2 == 1)
                buttons.SetActive(true);
            else
                buttons.SetActive(false);

        }

    }

    public void Ask()
    {
        SceneManager.LoadScene("Catapulta");
    }

    public void Back()
    {
        button1.SetActive(false);
    }

    public void Exit()
    {
        SceneManager.LoadScene("StartScene");
    }

    public void Backk()
    {
        button2.SetActive(false);
    }

    public void AcceptReload()
    {
        button1.SetActive(true);
    }

    public void AcceptExit()
    {
        button2.SetActive(true);
    }
}
