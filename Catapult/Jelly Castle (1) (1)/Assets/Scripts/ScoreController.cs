﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreController : MonoBehaviour
{
    public Text scoreText;
    public int score;
    public GameObject scoreTextt;
    private int counter;
    //public GameObject bais;
    //public GameObject[] cubes;


    void Start()
    {
        //cubes = GameObject.FindGameObjectsWithTag("ScoreCube");
        score = 0;
        counter = 0;
    }

    void Update()
    {
        if(score > 0)
            scoreText.text = "Your score: " + score;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            counter++;
            if (counter % 2 == 1)
                scoreTextt.SetActive(false);
            else
                scoreTextt.SetActive(true);

        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ScoreCube")
            score++;
    }
}
