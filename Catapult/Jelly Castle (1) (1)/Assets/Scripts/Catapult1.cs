﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Catapult1 : MonoBehaviour
{
    public float force;
    public float deltaForce;
    public Rigidbody rb;
    private int tryCounter;

    public Rigidbody orange;
    public Transform or;
    //public Text text;

    private int rotation;
    public GameObject silverCatapulta;


    public GameObject or1;
    public GameObject or2;
    public GameObject or3;

    void Start()
    {
        rotation = 0;
        force = 10;
        deltaForce = 0.5f;
        rb.GetComponent<Rigidbody>();
        tryCounter = 0;
        or1.SetActive(true);
        or2.SetActive(true);
        or3.SetActive(true);
    }

    void Update()
    {
        if (tryCounter == 0)
        {
            Instantiate(orange, or.position, or.rotation);
            tryCounter++;
        }

        Debug.Log(tryCounter);

        if (Input.GetKey(KeyCode.S))
        {
            if (force <= 20)
                force += deltaForce;
            rb.velocity = transform.up * -force / 10;
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            tryCounter++;
            rb.velocity = transform.up * force;
            force = 10;
        }

        if (Input.GetKey(KeyCode.D))
        {
            if (rotation < 10)
            {
                silverCatapulta.transform.Rotate(0, 3, 0, Space.Self);
                rotation++;
            }
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (rotation > -10)
            {
                silverCatapulta.transform.Rotate(0, -3, 0, Space.Self);
                rotation--;
            }
        }

        if (tryCounter <= 4)
        {
            if (tryCounter % 2 == 0 && tryCounter > 0)
            {
                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    Instantiate(orange, or.position, or.rotation);
                    tryCounter++;
                }
            }
        }
        //else
        //text.text += "\n" + "Press Esc to reload";

        switch (tryCounter)
        {
            case 2:
                or1.SetActive(false);
                break;
            case 4:
                or2.SetActive(false);
                break;
            case 6:
                or3.SetActive(false);
                break;
        }
    }



}
