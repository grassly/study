using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetAxis("Horizontal") > 0)
            rb.velocity = Vector3.right * speed;

        if (Input.GetAxis("Horizontal") < 0)
            rb.velocity = -Vector3.right * speed;

        if (Input.GetAxis("Vertical") > 0)
            rb.velocity = Vector3.forward * speed;

        if (Input.GetAxis("Vertical") < 0)
            rb.velocity = -Vector3.forward * speed;
    }


}
