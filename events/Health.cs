using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Health : MonoBehaviour
{
    [SerializeField]private float hp = 100;
    public float Hp => hp;
    private bool isAlive => hp > 0;


    private void GetDamage(float d)
    {
        if (isAlive)
            hp -= d;
    }

    private void Update()
    {
        Debug.Log(hp);
    }

    private void OnEnable()
    {
        DelegateManager.OnObstacleCollision += GetDamage;
    }

    private void OnDisable()
    {
        DelegateManager.OnObstacleCollision -= GetDamage;
    }
}
