using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Wallet : MonoBehaviour
{
    [SerializeField] private int coin;
    public int Coin => coin;


    private void GetMoney(int m)
    {
        coin += m;
    }


    private void OnEnable()
    {
        DelegateManager.OnCoinPickUp += GetMoney;
    }

    private void OnDisable()
    {
        DelegateManager.OnCoinPickUp -= GetMoney;
    }
}
