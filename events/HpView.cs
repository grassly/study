using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HpView : MonoBehaviour
{
    public Slider hpSlider;
    public Health h;

    void DrawHp(float n)
    {
        hpSlider.value = h.Hp / 100;
    }

    private void OnEnable()
    {
        DelegateManager.OnObstacleCollision += DrawHp;
    }

    private void OnDisable()
    {
        DelegateManager.OnObstacleCollision -= DrawHp;
    }
}
