using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelegateManager : MonoBehaviour
{
    public static System.Action<float> OnObstacleCollision;
    public static System.Action<int> OnCoinPickUp;


    private float damage = 20;
    //private int coin = 1;
    public bool inEnemy;


    public void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.tag)
        {
            case "Enemy":
                OnObstacleCollision?.Invoke(damage);
                return;

            case "Coin":
                int coin = other.gameObject.GetComponent<CurrCoinPrice>().cost;
                OnCoinPickUp?.Invoke(coin);
                Destroy(other.gameObject);
                return;
        }
    }

}
