using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class WalletView : MonoBehaviour
{
    public Wallet w;
    public Text coinText;

    void DrawWallet(int n)
    {
        coinText.text = $"Cash: {w.Coin}";
    }

    private void OnEnable()
    {
        DelegateManager.OnCoinPickUp += DrawWallet;
    }

    private void OnDisable()
    {
        DelegateManager.OnCoinPickUp -= DrawWallet;
    }
}
