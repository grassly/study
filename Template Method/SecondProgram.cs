using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondProgram : MonoBehaviour
{
    private OverridenB _overridenB = new OverridenB();

    [SerializeField] private string s;
    [SerializeField] private int a;
    [SerializeField] private int b;
    [SerializeField] private int c;

    void Start()
    {
        int res = _overridenB.TemplateMethod(s, a, b, c);
        Debug.Log(res);
    }
}
