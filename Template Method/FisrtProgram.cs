using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstProgram : MonoBehaviour
{
    private OverridenA _overridenA = new OverridenA();

    [SerializeField] private string s;
    [SerializeField] private int a;
    [SerializeField] private int b;
    [SerializeField] private int c;

    void Start()
    {
        int res = _overridenA.TemplateMethod(s, a, b, c);
        Debug.Log(res);
    }
}
