using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AClass
{
    public abstract int AbstractA(string s);
    public abstract int AbstractB();
    public virtual int VirtualA(int a, int b)
    {
        return a + b;
    }
    public virtual int VirtualB(int c)
    {
        return c * c;
    }

    public virtual int TemplateMethod(string s, int a, int b, int c)
    {
        return (int)Mathf.Pow(AbstractA(s), AbstractB()) + VirtualA(a, b) * VirtualB(c);
    }
}

public class OverridenA : AClass
{
    public override int AbstractA(string s)
    {
        return s.Length;
    }

    public override int AbstractB()
    {
        return 42;
    }
}

public class OverridenB : AClass
{
    public override int AbstractA(string s)
    {
        int vowelAmount = 0;
        string[] word = s.Split();
        for (int i = 0; i < word.Length; i++)
        {
            if (word[i] == "a" ||
                word[i] == "e" ||
                word[i] == "i" ||
                word[i] == "o" ||
                word[i] == "u" ||
                word[i] == "y")
            {
                vowelAmount++;
            }
        }
        return vowelAmount;
    }

    public override int AbstractB()
    {
        int someInt = Random.Range(1, 3);
        return someInt;
    }

    public override int VirtualA(int a, int b)
    {
        return a * b;
    }

    public override int VirtualB(int c)
    {
        return c % 7;
    }
}
