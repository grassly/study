﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SecondV : MonoBehaviour
{
    private int[] arr;
    public Text text;

    void Start()
    {
        int length = Random.Range(2, 10);
        int[] arr = new int[length];

        for (int i = 0; i < length; i++)
        {
            arr[i] = Random.Range(0, 10);
        }
        for (int i = length - 1; i > -1; i--)
        {
            text.text += arr[i] + " ";
        }
    }

    void Update()
    {
        
    }
}
