﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SixthV : MonoBehaviour
{
    private float[] arrAll = new float[20];
    private float[] pos;
    private float[] neg;
    private int posLength;
    private int neglength;

    void Start()
    {
        int m = 0;
        int n = 0;

        for (int i = 0; i < arrAll.Length; i++)
        {
            arrAll[i] = - UnityEngine.Random.Range(-9, 7);
            if (arrAll[i] > 0)
                posLength++;
            else if (arrAll[i] < 0)
                neglength++;
        }

        float[] pos = new float[posLength];
        float[] neg = new float[neglength];

        for (int i = 0; i < arrAll.Length; i++)
        {
            if (arrAll[i] > 0)
            {
                pos[m] = arrAll[i];
                m++;
            }
            else if (arrAll[i] < 0)
            {
                neg[n] = arrAll[i];
                n++;
            }
        }

        for (int i = 0; i < posLength; i++)
            Debug.Log(pos[i]);
        for (int i = 0; i < neglength; i++)
            Debug.Log(neg[i]);
    }

    void Update()
    {
        
    }
}
