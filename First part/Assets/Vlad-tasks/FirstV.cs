﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class FirstV : MonoBehaviour
{
    private int[] arr = new int[10];

    public Text text1;
    public Text text2;


    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            arr[i] = Random.Range(0, 5);
            text1.text += arr[i] + " ";
            if (arr[i] == 2)
                text2.text += 0 + " ";
            else
                text2.text += arr[i] + " ";
        }
    }

    void Update()
    {
        
    }
}
