﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Fifth : MonoBehaviour
{
    private int[] arr = new int[10];
    [SerializeField] private int customElement;
    [SerializeField] private int customElementNumber;
    [SerializeField] private Text text;

    void Start()
    {
        for (int i = 0; i < arr.Length - 1; i++)
        {
            arr[i] = UnityEngine.Random.Range(1, 39);
            text.text += arr[i] + " ";
            if (i == arr.Length - 2)
                text.text += "\n";
        }

        for (int i = 0; i < arr.Length; i++)
        {
            if (i < customElementNumber - 1)
                text.text += arr[i] + " ";
            else if (i == customElementNumber - 1)
                text.text += customElement + " ";
            else
                text.text += arr[i - 1] + " ";
            
        }

    }

    void Update()
    {
        
    }
}
