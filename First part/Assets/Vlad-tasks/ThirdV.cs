﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class ThirdV : MonoBehaviour
{
    private int[] arr = new int[10];
    private int max;

    private int sum;
    private int multiple;

    public Text text;

    void Start()
    {
        sum = 0;
        multiple = 1;

        max = -6;
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = Random.Range(-6, 10);
            text.text += arr[i] + " ";

            sum += arr[i];
            multiple *= arr[i]; 

            if (arr[i] < 0 && arr[i] > max)
                max = arr[i];
        }

        text.text += "\n" + sum + " " + multiple;

    Debug.Log(max);
    }

    void Update()
    {
        
    }
}
