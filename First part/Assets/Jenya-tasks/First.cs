﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class First : MonoBehaviour
{
    public int Level;
    public int FinishLevel;

    void Start()
    {
        if (Level < FinishLevel)
        {
            Debug.Log("Continue");
        }
        else {
            Debug.Log("Game Over");
        }
    }

    void Update()
    {
        
    }
}
