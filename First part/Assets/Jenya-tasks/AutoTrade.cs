﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoTrade : MonoBehaviour
{
    public float Cost;
    public bool Sold;
    public float Wallet;
    public float LastPrice;

    void Start()
    {
        Cost = 750;
        Sold = true;
        Wallet = 1000;
    }

    void Update()
    {
        Cost += Random.Range(-1, 2);
        Check(Cost);
    }

    public void Sell(float Price)
    {
        Sold = true;
        Debug.Log($"Мама, я продал за {Price}!");
        Wallet += Price;
        LastPrice = Cost;
    }

    public void Buy(float Price)
    {
        Sold = false;
        Debug.Log($"Мама, я купил за {Price}!");
        Wallet -= Price;
        LastPrice = Cost;
    }

    public void Check(float Price)
    {
        if ((Cost > LastPrice + 10 || Cost < LastPrice - 7) && Sold == false)
            Sell(Cost);
        else if ((Cost <= LastPrice + 3) && Sold == true)
            if (Cost <= Wallet)
                Buy(Cost);
    }
}
