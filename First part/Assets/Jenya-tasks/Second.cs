﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Second : MonoBehaviour
{
    public int Level;
    public int Exp;

    void Start()
    {
        
    }

    void Update()
    {
        Exp++;
        if (Exp % 350 == 0)
        {
            Level++;
            Debug.Log($"Достигнут уровень {Level}");
        }
    }
}
