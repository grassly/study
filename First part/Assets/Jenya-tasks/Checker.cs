﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;


    public class Checker : MonoBehaviour
    {
        public Car actualCar;
        public Car currentCar;


        public static string Passeenger = "passenger";
        public static string Truck = "truck";
        public static string Jeep = "jeep";

        public string[] mass = new string[3] { "passenger", "truck", "jeep" };

        public static string Red = "red";
        public static string Green = "green";
        public static string Gold = "gold";

        public string[] color = new string[3] { "red", "green", "gold" };

        public void Check()
        {
            Car currentCar = new Car();

            currentCar.Mass = mass[Random.Range(0, 3)];
            currentCar.Color = color[Random.Range(0, 3)];

            Car actualCar = new Car();

            actualCar.Mass = mass[Random.Range(0, 3)];
            actualCar.Color = color[Random.Range(0, 3)];

            if (currentCar.Mass == actualCar.Mass && currentCar.Color == actualCar.Color)
            {
                Debug.Log($"Проверка пройдена! {actualCar.Mass} {actualCar.Color}");
            }
            else
            {
                Debug.Log($"Проверка не пройдена :( \n {actualCar.Mass},  {actualCar.Color} vs {currentCar.Mass}, {currentCar.Color} ");
            }
        }

    }
