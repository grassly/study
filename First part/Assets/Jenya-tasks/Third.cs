﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Third : MonoBehaviour
{
    public float Distance;

    void Start()
    {
        Distance = 1;
    }

    void Update()
    {
        Debug.Log($"Было: {Distance}");
        if (Distance < -10 || Distance > 10)
        {
            Distance *= 0.9f;
        }
        else
        {
            if (Distance < 0)
            {
                Distance -= 1000;
            }
            else if (Distance > 0)
            {
                Distance += 1000;
            }
        }
        Debug.Log($"Стало: {Distance}");

    }
}
