﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Locator
{
    static IController controller;
    static SoundController soundController;

    public static void Init()   //текущий сервис - контроллер звука
    {
        controller = soundController;
    }

    public static void Provide(IController newController)   //смена сервиса, если их >1
    {
        controller = newController;
    }

    public static IController GetController()   //получение текущего сервиса
    {
        return controller;
    }
}