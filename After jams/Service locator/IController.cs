﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IController    //интерфейс, strategy
{
    void PlaySound(AudioSource playSound);
}

//мне помогал керил анатолич, так что совпадения с другими скриптами не случайны и вполне оправданы
