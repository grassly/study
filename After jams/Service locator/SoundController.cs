﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : IController  //strategy, реализация интерфейса
{
    public void PlaySound(AudioSource playSound)
    {
        playSound.Play();
    }
}
