﻿using UnityEngine;

public abstract class Item : ScriptableObject
{
    [SerializeField] private int _id;
    [SerializeField] private string _name;
    [SerializeField] private int _price;
    [SerializeField] private int _amount;
    [SerializeField] private GameObject prefab;

    public string Name => _name;
    public int Price => _price;
    public int Amount => _amount;
    public int Id => _id;

    private void OnValidate() //изменение в инспекторе
    {
        if (_price < 0)
            _price = 0;
        if (_amount < 0)
            _amount = 0;
    }
}

[CreateAssetMenu(menuName = "Thing")]
public class Thing : Item
{
    [SerializeField] private float _treasure;
    public float Treasure => _treasure;

    public float Price()
    {
        return _treasure;
    }
}

[CreateAssetMenu(menuName = "Food")]
public class Food : Item
{
    [SerializeField] private float _caloricity;
    public float Caloricity => _caloricity;

    public float Eat()
    {
        return _caloricity;
    }
}

[CreateAssetMenu(menuName = "Weapon")]
public class Weapon : Item
{
    [SerializeField] private float _damage;
    public float Damage => _damage;

    public float Hurt()
    {
        return _damage;
    }
}

