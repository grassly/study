﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float _enHp;

    public void TakeDamage(float damage)
    {
        _enHp -= damage;
    }
}
