﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ValueVisual : MonoBehaviour
{
    public Text text;

    public void Init(ObservableFloat f)
    {
        f.OnValueChanged += UpdateView;
    }

    public void UpdateView(float value)
    {
        text.text = value.ToString();
    }
}
