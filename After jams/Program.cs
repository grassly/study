﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Program : MonoBehaviour
{
    public delegate string Message(string msg);

    public delegate float Sum(float a, float b);
    public delegate float Dif(float a, float b);
    public delegate int Factor(int a);
    public delegate string StrokeSum(string a, string b);

    //public static void WriteDelegates(string delegates) { Debug.Log(delegates); }
    //public static string WriteCool(string a) => new string(a.Cool());  //?

    //Cool c = WriteCool;

    //Cool.Invoke("круто"); //?
    public void Start()
    {
        WriteConsole();
        Compute();
        Summorize();
    }

    private static void WriteConsole()
    {
        Message m = Text;
        //Message m1 = Text;
        Debug.Log(m("Делегаты"));
        Debug.Log(m("круто"));
        Debug.Log(m("Делегаты") + m("круто"));
    }

    private static void Compute()
    {
        Sum s = CalculateSum;
        Dif d = CalculateDif;
        Factor f = delegate(int a) 
        {
            int factorial = 1;
            for (int i = 2; i <= a; i++)
            {
                factorial *= i;
            }
            return factorial;
        };
        Debug.Log($"Sum: {s(5, 8)}");
        Debug.Log($"Dif: {d(5, 8)}");
        Debug.Log($"Factor: {f(8)}");
    }

    private static void Summorize()
    {
        StrokeSum ss = (a,b) => a + b;
        Debug.Log(ss("dele", "gate!"));
    }

    private static string Text(string txt) => txt;
    private static float CalculateSum(float a, float b) => a + b;
    private static float CalculateDif(float a, float b) => a - b;
    /*private static int CalculateFactor(int a)
    {
        int factorial = 1;
        for (int i = 2; i <= a; i++)
        {
            factorial *= i;
        }
        return factorial;
    }*/
    //private static string Summary(string a, string b) => a + b;
}