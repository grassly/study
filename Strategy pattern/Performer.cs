using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StrategyPattern
{
    public interface IStrategy
    {
        void Perform(Transform t, float param);
    }

    public class MoveForward : IStrategy
    {
        public void Perform(Transform t, float speed)
        {
            t.position += Vector3.right * speed * Time.deltaTime;
        }
    }

    public class Rotate : IStrategy
    {
        public void Perform(Transform t, float rotationSpeed)
        {
            t.Rotate(0, 0, rotationSpeed * Time.deltaTime, Space.World);
        }
    }

    public class Emit : IStrategy
    {
        public void Perform(Transform t, float particleAmount)
        {
            ParticleSystem system = t.GetComponentInChildren<ParticleSystem>();
            system.Emit((int)particleAmount);
        }
    }

    public class Performer : MonoBehaviour
    {
        private IStrategy strategy { get; set; }

        public float speed;
        public float rotationSpeed;
        public float particleAmount;


        public void Awake()
        {
            strategy = new Emit();
        }

        public void SetStrategy(IStrategy newStrategy)
        {
            strategy = newStrategy;
        }

        private void Update()
        {
            switch (strategy.ToString())
            {
                case "StrategyPattern.Emit":
                    strategy.Perform(gameObject.transform, particleAmount);
                    break;
                case "StrategyPattern.MoveForward":
                    strategy.Perform(gameObject.transform, speed);
                    break;
                case "StrategyPattern.Rotate":
                    strategy.Perform(gameObject.transform, rotationSpeed);
                    break;
            }
            Debug.Log(strategy.ToString());
        }

        public void OnClickButtonMove()
        {
            SetStrategy(new MoveForward());
            //Debug.Log("move");
        }
        
        public void OnClickButtonRotate()
        {
            SetStrategy(new Rotate());
            //Debug.Log("rotate");
        }

        public void OnClickButtonEmit()
        {
            SetStrategy(new Emit());
            //Debug.Log("emit");
        }

    }
}
